import tensorflow as tf

node1 = tf.constant(3.0, name="datph")
node2 = tf.constant(4.0)
node3 = tf.add(node1, node2)
print(node1, node2)

a = tf.placeholder(tf.float32)
b = tf.placeholder(tf.float32)
added_node = a + b

# with tf.Session() as sess:
#     print(sess.run(node3))
#     add_and_triple = added_node * 3
#     print(sess.run(add_and_triple, {a: 3, b: 4.5}))

W = tf.Variable([.3], dtype=tf.float32)
b = tf.Variable([-.3], dtype=tf.float32)
x = tf.placeholder(tf.float32)

linear_model = W * x + b
# with tf.Session() as sess:
#     init = tf.global_variables_initializer()
#     sess.run(init)
#     print(sess.run(linear_model, {x: [[1], [2], [3], [4], [5]]}))
y = tf.placeholder(tf.float32)
# squared_deltas = tf.square(linear_model -y)
# loss = tf.reduce_sum(squared_deltas)
# fixW = tf.assign(W, [-1.])
# fixb = tf.assign(b, [1.])
# with tf.Session() as sess:
#     init = tf.global_variables_initializer()
#     sess.run(init)
#     sess.run([fixW, fixb])
#     print(sess.run(loss, {x: [1, 2, 3, 4], y: [0, -1, -2, -3]}))
# loss
loss = tf.reduce_sum(tf.square(linear_model - y)) # sum of the squares
# optimizer
optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)

# training data
x_train = [1, 2, 3, 4]
y_train = [0, -1, -2, -3]
# training loop
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init) # reset values to wrong
for i in range(1000):
  sess.run(train, {x: x_train, y: y_train})

# evaluate training accuracy
curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
print("W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))
