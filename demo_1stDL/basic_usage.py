# import tensorflow as tf
import numpy as np

# arr = [1, 2, 3, 4, 5, 6, 7]
# arr2d = [[1, 1, 1, 1, 1], [2, 2, 2, 2, 2]]
# print(arr2d)
# print(arr)
#
# a = np.arange(15).reshape(3, 5)
# print(a)
# print(a.shape)
# print(a.ndim)
#
# b = np.array(arr2d)
# print(b)
# print(b.dtype)
#-----------------------------------------------------------------
# matrix1 = tf.constant([3, 3, 4, 5, 6, 7])
# print(matrix1)
# matrix3 = tf.constant([[2.], [2.]])
# matrix2 = tf.constant([[3., 3.]])
# print(matrix2)
#
# product = tf.matmul(matrix2, matrix3)
#
# with tf.Session() as sess:
#     with tf.device("/cpu:0"):
#         result = sess.run(product)
#         print(result)
#
#
# input1 = tf.constant([3.0])
# print(input1)
# input2 = tf.constant([2.0])
# print(input2)
# input3 = tf.constant([5.0])
# print(input3)
# intermed = tf.add(input2, input3)
# print(intermed)
# # mul = tf.matmul(input1, intermed)
#
# with tf.Session() as sess:
#     result = sess.run(intermed)
#     print(result)
#-----------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt


def mandelbrot(h, w, maxit=20):
    """Returns an image of the Mandelbrot fractal of size (h,w)."""
    y, x = np.ogrid[ -1.4:1.4:h*1j, -2:0.8:w*1j ]
    c = x+y*1j
    z = c
    divtime = maxit + np.zeros(z.shape, dtype=int)

    for i in range(maxit):
        z = z**2 + c
        diverge = z*np.conj(z) > 2**2            # who is diverging
        div_now = diverge & (divtime == maxit)  # who is diverging now
        divtime[div_now] = i                  # note when
        z[diverge] = 2                        # avoid diverging too much

    return divtime


plt.imshow(mandelbrot(400, 400))
plt.show()

