from __future__ import print_function
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from six.moves import cPickle as pickle

data_root = 'D:\DemoDL\ASUdacity' # Change me to store data elsewhere
pickle_file = os.path.join(data_root, 'notMNIST.pickle')


def load_set_batch(filename, set_type='training', size=-1):
    with open(filename, 'rb') as f:
        if sys.version_info[0] < 3:
            dict = pickle.load(f)
        else:
            dict = pickle.load(f, encoding='latin1')

        if set_type == 'training':
            data_name = 'train_dataset'
            label_name = 'train_labels'
        elif set_type == 'valid':
            data_name = 'valid_dataset'
            label_name = 'valid_labels'
        elif set_type == 'test':
            data_name = 'test_dataset'
            label_name = 'test_labels'

        labels = dict[label_name]
        data = dict[data_name]
    return labels[:size], data[:size]


labels, train_set = load_set_batch(pickle_file, size=50)
logregr = LogisticRegression(fit_intercept=False)
logregr.fit(train_set, labels)

labels_test, test_set = load_set_batch(pickle_file, 'test', 20)
pre = logregr.predict(test_set)
print('Prediction (50): ', pre)
print(logregr.score(test_set, labels_test))


labels, train_set = load_set_batch(pickle_file, size=500)
logregr = LogisticRegression(fit_intercept=False)
logregr.fit(train_set, labels)

pre = logregr.predict(test_set)
print('Prediction (500): ', pre)
print(logregr.score(test_set, labels_test))


# labels, train_set = load_set_batch(pickle_file, size=5000)
# logregr = LogisticRegression(fit_intercept=False)
# logregr.fit(train_set, labels)
#
# pre = logregr.predict(test_set)
# print('Prediction (5000): ', pre)
# print(logregr.score(test_set, labels_test))

fig = plt.figure()

for i in range(20):
    subplot = fig.add_subplot(5, 4, i + 1)
    image = test_set[i]
    image = np.reshape(image, (28, 28))
    imgplt = plt.imshow(image, cmap='gray')
    subplot.set_title(labels_test[i])

plt.show()

# A_data_name = train_datasets[0]
# with open(A_data_name, 'rb') as f:
#     dict = pickle.load(f, encoding='latin1')
#
# random_n = np.random.randint(0, len(dict))
# image_data = dict[random_n]
# image_data = np.reshape(image_data, (28, 28))
#
# plt.imshow(image_data, cmap='gray')
# plt.show()