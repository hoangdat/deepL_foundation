# digits = datasets.load_digits()
#
# #digits.data is the actual data (features)
# print(digits.images[1])
#
# #digits.target is the actual label we've assigned to the digits data.
# print(digits.target[1])
#
# #specify the classifier
# #clf = svm.SVC(gamma = 0.001, C= 100)
# #clf = KNeighborsClassifier()
# #clf = LogisticRegression()
# clf = DecisionTreeClassifier()
#
# #This loads in all but the last 10 data points ->
# #use the last 20 data points for testing.
# #The X contains all of the "coordinates"
# #y is simply the "target" or "classification" of the data
# X,y = digits.data[:-20], digits.target[:-20]
#
#
# #training
# clf.fit(X,y)
#
# #testing
# percent = 0
# for i in range(1, 21):
#     pre = clf.predict(digits.data[-i])
#     print(pre)
#     if pre == digits.target[-i]:
#         percent += 1
#     else:
#         print('wrong: ', digits.target[-i])
#     plt.imshow(digits.images[-i], cmap = plt.cm.gray_r, interpolation = 'nearest')
#     plt.show()
#
# print(percent*100/20)
# X1, y1 = digits.data[-20:], digits.target[-20:]
# pre = clf.predict(X1)
# print(clf.score(X1,y1))
