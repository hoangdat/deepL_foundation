import numpy as np
# vector = np.array([5, 10, 15, 20])
# vector_10 = vector == 10
# print(vector_10)


# comparision element wise
# countries_canada = world_alcohol[:, 2] == 'Canada'
# years_1984 = world_alcohol[:, 0] == '1984'

# boolean index in vector
# vector = np.array([5, 10, 15, 20])
# equal_to_ten = (vector == 10)
#
# print(vector[equal_to_ten])
# # ==> 10
#
#
# matrix = np.array([
#                 [5, 10, 15],
#                 [20, 25, 30],
#                 [35, 40, 45]
#              ])
# second_column_25 = (matrix[:,1] == 25)
# print(second_column_25)
# print(matrix[second_column_25, :])

# Creates matrix.
# Uses second_column_25 to select any rows in matrix where second_column_25 is True.
#
# is_algeria_and_1986 = (world_alcohol[:, 0] == '1986') & (world_alcohol[:, 2] == 'Algeria')
# rows_with_algeria_and_1986 = world_alcohol[is_algeria_and_1986, :]

matrix = np.array([
                [5, 10, 15],
                [20, 25, 30],
                [35, 40, 45]
             ])
# print(matrix.sum(axis=1))

# a = matrix[1:, 1:]
# print(a)
# a = a[:, 1:]
# print(a)
#
mean = matrix.mean(axis=0)
print(mean)

min_index = 0
max_index = 200000
lookback = 1024
batch_size = 128

rows = np.random.randint(
                min_index + lookback, max_index, size=batch_size)

print(rows)

a = range(2000 - 1024, 2000, 6)
for i in a:
    print(i)