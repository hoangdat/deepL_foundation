import os, shutil

original_dataset_dir = 'D:\DemoDL\KaggleData\\train'
base_dir = 'D:\DemoDL\KaggleData\cats_and_dogs'

train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)
valid_dir = os.path.join(base_dir, 'valid')
os.mkdir(valid_dir)
test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

train_cat_dir = os.path.join(train_dir, 'cats')
os.mkdir(train_cat_dir)
train_dog_dir = os.path.join(train_dir, 'dogs')
os.mkdir(train_dog_dir)

valid_cat_dir = os.path.join(valid_dir, 'cats')
os.mkdir(valid_cat_dir)
valid_dog_dir = os.path.join(valid_dir, 'dogs')
os.mkdir(valid_dog_dir)

test_cat_dir = os.path.join(test_dir, 'cats')
os.mkdir(test_cat_dir)
test_dog_dir = os.path.join(test_dir, 'dogs')
os.mkdir(test_dog_dir)

fnames = ['cat.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(train_cat_dir, fname)
    shutil.copyfile(src, dst)

fnames = ['cat.{}.jpg'.format(i) for i in range(1000, 1500)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(valid_cat_dir, fname)
    shutil.copyfile(src, dst)

fnames = ['cat.{}.jpg'.format(i) for i in range(1500, 2000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(test_cat_dir, fname)
    shutil.copyfile(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(train_dog_dir, fname)
    shutil.copyfile(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1000, 1500)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(valid_dog_dir, fname)
    shutil.copyfile(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1500, 2000)]
for fname in fnames:
    src = os.path.join(original_dataset_dir, fname)
    dst = os.path.join(test_dog_dir, fname)
    shutil.copyfile(src, dst)

print('total training cat images:', len(os.listdir(train_cat_dir)))
print('total training dog images:', len(os.listdir(train_dog_dir)))
print('total validation cat images:', len(os.listdir(valid_cat_dir)))
print('total validation dog images:', len(os.listdir(valid_dog_dir)))
print('total test cat images:', len(os.listdir(test_cat_dir)))
print('total test dog images:', len(os.listdir(test_dog_dir)))
