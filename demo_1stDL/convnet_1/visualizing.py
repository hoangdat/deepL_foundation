from keras.models import load_model
from keras.preprocessing import image
from keras import models

import numpy as np
import os
import matplotlib.pyplot as plt

base_dir = 'D:\DemoDL\KaggleData\cats_and_dogs'

train_dir = os.path.join(base_dir, 'train')
valid_dir = os.path.join(base_dir, 'valid')
test_dir = os.path.join(base_dir, 'test')

model = load_model('cats_and_dogs_small_1.h5')
model.summary()

img_path = os.path.join(test_dir, 'cats/cat.1700.jpg')
img = image.load_img(img_path, target_size=(150, 150))
img_tensor = image.img_to_array(img)
print('first tensor: ', img_tensor.shape)
img_tensor = np.expand_dims(img_tensor, axis=0)
img_tensor /= 255.

print(img_tensor.shape)

plt.imshow(img_tensor[0])
plt.show()

layer_output = [layer.output for layer in model.layers[:8]]
activation_model = models.Model(inputs=model.input, outputs=layer_output)

activations = activation_model.predict(img_tensor)
# print(len(activations))
#
# first_layer_activation = activations[0]
# print(first_layer_activation.shape)
#
# plt.matshow(first_layer_activation[0, :, :, 10], cmap='viridis')
# plt.show()
#
# eight_layer_activation = activations[7]
# print(eight_layer_activation.shape)
# plt.matshow(eight_layer_activation[0, :, :, 10], cmap='viridis')
# plt.show()

layer_names = []
for layer in model.layers[:8]:
    layer_names.append(layer.name)

image_per_row = 16
for layer_name, layer_activation in zip(layer_names, activations):
    n_feature = layer_activation.shape[-1]

    size = layer_activation.shape[1]
    n_row = n_feature // image_per_row

    display_grid = np.zeros((size * n_row, image_per_row * size))

    for row in range(n_row):
        for col in range(image_per_row):
            channel_image = layer_activation[0, :, :, row * image_per_row + col]
            channel_image -= channel_image.mean()
            std = channel_image.std()
            print('std: ', std)
            # channel_image /= std
            channel_image *= 64
            channel_image += 128
            channel_image = np.clip(channel_image, 0, 255).astype('uint8')
            display_grid[row * size: (row + 1) * size,
                         col * size: (col + 1) * size] = channel_image

    scale = 1. / size
    plt.figure(figsize=(scale * display_grid.shape[1],
                        scale * display_grid.shape[0]))
    plt.title(layer_name)
    plt.grid(False)
    plt.imshow(display_grid, aspect='auto', cmap='viridis')

plt.show()

