import numpy as np
import string
from keras.preprocessing.text import Tokenizer

samples = ['The cat sat on the mat.', 'The dog ate my homework.']
token_index = {}
for sample in samples:
    for word in sample.split():
        if word.lower() not in token_index:
            token_index[word.lower()] = len(token_index) + 1

print(token_index)

max_length = 10
print("========================================================")
results = np.zeros(shape=(len(samples),
                          max_length,
                          max(token_index.values()) + 1))
for i, sample in enumerate(samples):
    sample = sample.lower()
    print(list(enumerate(sample.split())))
    for j, word in list(enumerate(sample.split())):
        print(j, word)
        index = token_index.get(word)
        results[i, j, index] = 1.
    print("------------------------")

print(results[1])

# samples = ['The cat sat on the mat.', 'The dog ate my homework.']
# characters = string.printable
# print(characters)
#
# token_index = dict(zip(range(1, len(characters) + 1), characters))
# print(token_index)
#
# max_length = 50
# results = np.zeros((len(samples), max_length, max(token_index.keys()) + 1))
#
# for i, sample in enumerate(samples):
#     for j, character in enumerate(sample):
#         index = token_index.get(character)
#         results[i, j, index] = 1.
#
# print(results[1].shape)
# print(results[1])

tokenizer = Tokenizer(num_words=50, char_level=False)
tokenizer.fit_on_texts(samples)
print(tokenizer)

sequences = tokenizer.texts_to_sequences(samples)
print(sequences)

one_hot_results = tokenizer.texts_to_matrix(samples, mode='binary')

word_index = tokenizer.word_index
print(word_index)
print('Found %s unique tokens.' % len(word_index))
print(one_hot_results[1].shape)
print(one_hot_results[1])
