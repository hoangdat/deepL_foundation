#comprehension

# >>> [ord(x) for x in 'spaam'] # List of character ordinals
# [115, 112, 97, 97, 109]
# >>> {ord(x) for x in 'spaam'} # Sets remove duplicates
# {112, 97, 115, 109}
# >>> {x: ord(x) for x in 'spaam'} # Dictionary keys are unique
# {'p': 112, 'a': 97, 's': 115, 'm': 109}
# >>> (ord(x) for x in 'spaam') # Generator of values
# <generator object <genexpr> at 0x000000000254DAB0>

# D = {'food': 'spam', 'quantity': 4, 'color': 'pink'}
# print(D)
# D.keys()
# if not 'food' in D:
#     print('missing food key')
#
# elif not 4 in D:
#     print('missing 4 value')
#
# sorted(D)
# print(D)


# def changeme(mylist):
#     mylist.append(198)
#     mylist = [1, 2, 3, 4]
#     mylist.append(199)
#     print("Cac gia tri ben trong ham la: ", mylist)
#     return
#
#
# mylist_d = [10, 20, 30]
# print('cac gia tri ben ngoai ham TRUOC la: ', mylist_d)
# changeme(mylist_d)
# print('cac gia tri ben ngoai ham SAU la: ', mylist_d)


# def msg(Id, Ten, Age=21):
#     "In gia tri da truyen"
#     print(Id)
#     print(Ten)
#     print(Age)
#     return
#
#
# # Function call
# msg(Id=100, Ten='Hoang', Age=20)
# msg(101, 'Thanh')

# Money = 2000
#
#
# def add_money():
#     # Uncomment the following line to fix the code:
#     global Money
#     Money = Money + 1
#
#
# print(Money)
# add_money()
# print(Money)


# class Employee:
#     'Common base class for all employees'
#     empCount = 0
#
#     def __init__(self, name, salary):
#         self.name = name
#         self.salary = salary
#         Employee.empCount += 1
#
#     def display_count(self):
#         print("Total Employee %d" % Employee.empCount)
#
#     def display_employee(self):
#         print("Name : ", self.name, ", Salary: ", self.salary)
#
#     def get_name(self):
#         if hasattr(self, 'name'):
#             print(self.name)
#         else:
#             print('not name attr')
#
#
# emp1 = Employee("Zara", 2000)
# "This would create second object of Employee class"
# emp2 = Employee("Manni", 5000)
# emp1.display_employee()
# emp2.display_employee()
# delattr(emp1, 'name')
# emp1.get_name()
# print("Total Employee %d" % Employee.empCount)
# s = 'spam'
# for i in range(len(s)):
#     s = s[1:] + s[:1]
#     print(s, end=' ')
import csv
nfl_file = 'nfl.csv'


class Dataset:
    def __init__(self, data):
        self.header = data[0]
        self.data = data[1:]

    def print_data(self):
        # New method **remember to add self**.
        print(self.data[:10])

    def column(self, col_name):
        if col_name not in self.header:
            return None

        index = 0
        for idx, value in enumerate(self.header):
            if col_name == value:
                index = idx
                break

        column = []
        for row in self.data:
            column.append(row[index])

        return column

    def column_unique(self, label):
        unique = set(self.column(label))
        total = 0
        if unique is None:
            return total
        total = len(unique)
        return total
        


f = open(nfl_file, 'r')
reader = csv.reader(f)
nfl_data = list(reader)
nfl_dataset = Dataset(nfl_data)
nfl_dataset = Dataset(nfl_data)
total_years = nfl_dataset.column_unique('boy')
print(total_years)
