class C2:
    def __init__(self, name):
        self.name = name

    def get_info(self):
        print('info: ', self.name)


class C3:
    def __init__(self, job):
        self.job = job

    def get_info(self):
        print('job: ', self.job)


class C1(C2, C3):
    def __init__(self, name, job):
        self.name = name
        self.job = job

    # def get_info(self):
    #     print('C1 name: %s, job: %s' % (self.name, self.job))

    # def get_info(self, age):
    #     print('C1 name: %s, job: %s, age: %s' % (self.name, self.job, age))


# c1 = C1('dat', 'IT')
# # c1.get_info()
# # c1.get_job()
# # c1.get_info(27)
# c2 = C2('bon')
# c3 = C3('audit')
# family = [c1, c2]
# for c in family:
#     c.get_info()


class FirstClass:
    def set_data(self, value):  # Define class's methods
        self.data = value  # self is the instance

    def display(self):
        print(self.data)


class SecondClass(FirstClass):
    def display(self):
        print('Current value = "%s"' % self.data)


class ThirdClass(SecondClass):
    def __init__(self, value):
        self.data = value

    def __add__(self, other):
        return ThirdClass(self.data + other)

    def __str__(self):
        return '[ThirdClass: %s]' % self.data

    def mul(self, other): # In-place change: named
        self.data *= other


x = FirstClass()
y = FirstClass()
x.set_data("king")
y.set_data(3.14)
x.data = 'queen'
x.display()

z = ThirdClass('cake')
z.display()
print(z)
b = z + 'xyz'    # __add__
b.display()
print(b)
