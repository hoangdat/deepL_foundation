from __future__ import print_function
import numpy as np
from six.moves import cPickle as pickle
from six.moves import range
from keras import models
from keras import optimizers
from keras import layers
from keras import losses
from keras import metrics
import random
import matplotlib.pyplot as plt
from keras import backend as K
K.tensorflow_backend._get_available_gpus()


with open('SVHN_train.pickle', 'rb') as f:
    tmp_save = pickle.load(f)
    train_dataset_56 = tmp_save['dataset']
    train_labels = tmp_save['labels']

valid_size = 2000
valid_dataset_56 = train_dataset_56[:valid_size]
valid_labels = train_labels[:valid_size]
train_dataset_56 = train_dataset_56[valid_size:]
train_labels = train_labels[valid_size:]

with open('SVHN_test.pickle', 'rb') as f:
    tmp_save = pickle.load(f)
    test_dataset_56 = tmp_save['dataset']
    test_labels = tmp_save['labels']

print('Training set', train_dataset_56.shape, train_labels.shape)
print('Validation set', valid_dataset_56.shape, valid_labels.shape)
print('Test set', test_dataset_56.shape, test_labels.shape)

num_digits = 5


def max_digits(dataset, labels, max_digits):
    keep = [i for i, label in enumerate(labels) if len(label) <= max_digits]
    return dataset[keep], labels[keep]


train_dataset_56, train_labels = max_digits(train_dataset_56, train_labels, num_digits)
valid_dataset_56, valid_labels = max_digits(valid_dataset_56, valid_labels, num_digits)
test_dataset_56, test_labels = max_digits(test_dataset_56, test_labels, num_digits)
print('Training set', train_dataset_56.shape, train_labels.shape)
# print('datph training set label', train_labels[3].shape) (3,1)
print('Validation set', valid_dataset_56.shape, valid_labels.shape)
print('Test set', test_dataset_56.shape, test_labels.shape)


def show_image(img, label):
    print("Labels", label)
    print("Dtype", img.dtype)
    print("Shape", img.shape)
    print("Color range", np.min(img), np.max(img))
    if len(img.shape) > 2:
        plt.imshow(np.reshape(img, img.shape[:2]))
    else:
        plt.imshow(img)
    plt.show()


def show_images(imgs, labels, num=3):
    for i in range(num):
        num = np.random.randint(imgs.shape[0])
        show_image(imgs[num], labels[num])


# for dataset, labels in [(train_dataset_56, train_labels),
#                         (valid_dataset_56, valid_labels),
#                         (test_dataset_56, test_labels)]:
#     show_images(dataset, labels, 2)


num_labels = 11
num_channels = 1

# print("test;;;;;;;;;;;;;;;;;;;;;;;;;;;")
# # print(list(valid_dataset_56.shape) + [1])
# row = np.array([[1]])
# print(row)
# print(row1.shape)
# # c = [num_labels-1] * 0
# # print(c)
# b = np.array([num_labels-1]) * (num_digits - len(row1))
# print(b)
# print(b.shape)
# # print("test;;;;;;;;;;;;;;;;;;;;;;;;;;; - test")
# t = np.append(row1, np.array([b]), 0)
# print(t)
#
# print(row.shape)
# print("aaaaaaaaaaa")
# c = np.ones((num_digits-len(row), 1))*(num_labels-1)
# print(c.shape)
# print(c)
# for row1 in train_labels[:5]:
#     print("row1:", row1)
#     a_0 = np.array([(np.arange(num_labels) == l).astype(np.float32)
#                     for l in np.append(row1, np.ones((num_digits-len(row1), 1))*(num_labels-1), 0)])
#     print(a_0)

# a_0 = np.array([(np.arange(num_labels) == l).astype(np.float32)
#                 for l in np.append(row1, [[num_labels-1] * (num_digits-len(row1))], 0)])
# # a_1 = np.append(row, [[num_labels-1] * (num_digits - len(row))], 0)
# # a_2 = np.array([np.array([(np.arange(num_labels) == l).astype(np.float32)
# #                         for l in np.append(row, [num_labels-1] * (num_digits - len(row)), 0)])
# #                         for row in labels])
# a_1 = np.array([np.array([(np.arange(num_labels) == l).astype(np.float32)
#                 for l in np.append(row, [num_labels-1] * (num_digits-len(row)), 0)])
#                 for row in train_labels])

# print(a_0)


def reformat(dataset, labels):
    dataset_output = dataset.reshape(list(dataset.shape) + [1]).astype(np.float32)
    labels_output = np.array([np.array([(np.arange(num_labels) == l).astype(np.float32)
                        for l in np.append(row, np.ones((num_digits-len(row), 1))*(num_labels-1), 0)])
                        for row in labels])
    return dataset_output, labels_output
# Training set (26259, 56, 56, 1) (26259, 3, 11)
# Validation set (2000, 56, 56, 1) (2000, 3, 11)
# Test set (13068, 56, 56, 1) (13068, 5, 11)
# Labels [[ 0.  0.  0.  0.  1.  0.  0.  0.  0.  0.  0.]
#  [ 0.  0.  0.  0.  0.  0.  1.  0.  0.  0.  0.]
#  [ 0.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.]]


train_dataset_56, train_labels = reformat(train_dataset_56, train_labels)
valid_dataset_56, valid_labels = reformat(valid_dataset_56, valid_labels)
test_dataset_56, test_labels = reformat(test_dataset_56, test_labels)


print('Training set', train_dataset_56.shape, train_labels.shape)
print('Validation set', valid_dataset_56.shape, valid_labels.shape)
print('Test set', test_dataset_56.shape, test_labels.shape)


# for dataset, labels in [(train_dataset_56, train_labels)]:
#     show_images(dataset, labels, 2)

# test_dataset_56 = test_dataset_56[:6000]
# test_labels = test_labels[:6000]
#
# train_dataset_56 = train_dataset_56
train_dataset_28 = train_dataset_56[:, ::2, ::2, :]
valid_dataset_28 = valid_dataset_56[:, ::2, ::2, :]
test_dataset_28 = test_dataset_56[:, ::2, ::2, :]
print('Training set', train_dataset_28.shape, train_labels.shape)
print('Validation set', valid_dataset_28.shape, valid_labels.shape)
print('Test set', test_dataset_28.shape, test_labels.shape)


dataset_56 = (train_dataset_56, valid_dataset_56, test_dataset_56)
dataset_28 = (train_dataset_28, valid_dataset_28, test_dataset_28)


# def accuracy(predictions, labels):
#     return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
#           / predictions.shape[0])
#
#
# def accuracy_list(predictions, labels):
#     result = np.mean([accuracy(predictions[i],
#                                labels[:, i, :])
#                         for i in range(num_digits)])
#     return result
kernel_size = 5
depth1 = 16
depth2 = 32
depth3 = 64


def run_model(model, train_dataset, labels, valid_dataset, v_labels):
    print("run model")
    history = model.fit(train_dataset, labels,
                        epochs=1, batch_size=128,
                        validation_data=(valid_dataset, v_labels))
    return history


def create_model_1(test_dataset,
                 use_dropout=False,
                 use_max_pool=False,
                 lr=0.05):
    img_height, img_width = test_dataset[0].shape[:2]
    model = models.Sequential()

    # # conv1
    # model.add(layers.Conv2D(depth1, (kernel_size, kernel_size), activation='relu',
    #                         input_shape=(img_height, img_width, num_channels)))
    #
    # if use_max_pool:
    #     model.add(layers.MaxPooling2D((2, 2)))
    #
    # # conv2
    # model.add(layers.Conv2D(depth2, (kernel_size, kernel_size), activation='relu'))
    #
    # if use_max_pool:
    #     model.add(layers.MaxPooling2D((2, 2)))
    #
    # # conv3
    # model.add(layers.Conv2D(depth3, (kernel_size, kernel_size), activation='relu'))
    for i in range(2):
        depth = 16 * (2 ** i)
        if i == 0:
            model.add(layers.Conv2D(depth, (kernel_size, kernel_size),
                                    activation='relu', input_shape=(28, 28, 1)))
        else:
            model.add(layers.Conv2D(depth, (kernel_size, kernel_size), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Dropout(0.7))
    model.add(layers.Flatten())
    model.add(layers.Dense(512, activation='relu'))
    model.add(layers.Dense(512, activation='relu'))
    model.add(layers.Dense(num_digits * 11))
    model.add(layers.Reshape((num_digits, 11)))
    model.add(layers.Activation('softmax'))

    model.compile(optimizer=optimizers.Adam(lr=0.05),
                  loss=losses.categorical_crossentropy,
                  metrics=['accuracy'])

    model.summary()
    return model


train_, valid_, test_ = dataset_28
model = create_model_1(test_)
history_ = run_model(model, train_, train_labels, valid_, valid_labels)

# img_batch = np.expand_dims(np.expand_dims(img_, 0), -1)
pred = model.predict(test_)
test_loss, test_acc = model.evaluate(test_, test_labels)
print("t_l: ", test_loss)
print("t_a: ", test_acc)
# for i in range(10):
#     print("PRE:")
#     print(pred[i])
#     print(pred[i].shape)
#
#     img_ = test_[i]
#     label_ = test_labels[i]
#     show_image(img_, label_)

print("Finish all")
