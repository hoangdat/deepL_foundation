from keras import models
from keras import layers
from keras import optimizers
from keras import losses
from keras import metrics
from keras.datasets import imdb
import numpy as np
import matplotlib.pyplot as plt

(train_data, train_label), (test_data, test_label) = imdb.load_data(num_words=10000)
print(train_data.shape)

word_index = imdb.get_word_index()
# print(word_index)
reversed_word_index = dict([(value, key) for (key, value) in word_index.items()])
# print(reversed_word_index)
decode_review = ' '.join([reversed_word_index.get(i-3, '?') for i in train_data[0]])
print(decode_review)

# print(len(train_data))
# model = models.Sequential()
# model.add(layers.Dense(16, activation='relu', input_shape=(10000,)))
# model.add(layers.Dense(16, activation='relu'))
# model.add(layers.Dense(1, activation='sigmoid'))


def one_hot_encoding(data, dimension=10000):
    result = np.zeros((len(data), dimension), dtype=np.float32)
    for i, sequence in enumerate(data):
        # print(i, sequence)
        result[i, sequence] = 1
    return result


x_train = one_hot_encoding(train_data)
x_test = one_hot_encoding(test_data)

# y_train = np.asarray(train_label).astype('float32')
# y_test = np.asarray(test_label).astype('float32')
#
# model = models.Sequential()
# model.add(layers.Dense(16, activation='relu', input_shape=(10000, )))
# model.add(layers.Dense(16, activation='relu'))
# model.add(layers.Dense(1, activation='sigmoid'))
#
# model.compile(optimizer=optimizers.RMSprop(lr=0.001),
#               loss=losses.mse,
#               metrics=[metrics.binary_accuracy])
#
# x_val = x_train[:10000]
# partial_x_train = x_train[10000:]
#
# y_val = y_train[:10000]
# partial_y_train = y_train[10000:]
#
# history = model.fit(partial_x_train, partial_y_train, epochs=20, batch_size=512, validation_data=(x_val, y_val))
# history_dict = history.history
# print(history_dict)
#
# loss_values = history_dict['loss']
# val_loss_values = history_dict['val_loss']
#
# epochs = range(1, len(loss_values) + 1)
#
# plt.plot(epochs, loss_values, 'bo', label='Training loss')
# plt.plot(epochs, val_loss_values, 'b', label='Validation loss')
# plt.title('Training and validation loss')
# plt.xlabel('Epochs')
# plt.ylabel('Loss')
# plt.legend()
# plt.show()
#
# plt.clf()
# acc_values = history_dict['binary_accuracy']
# val_acc_values = history_dict['val_binary_accuracy']
# plt.plot(epochs, acc_values, 'bo', label='Training acc')
# plt.plot(epochs, val_acc_values, 'b', label='Validation acc')
# plt.title('Training and validation accuracy')
# plt.xlabel('Epochs')
# plt.ylabel('Loss')
# plt.legend()
# plt.show()
