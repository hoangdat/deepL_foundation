from __future__ import print_function
import numpy as np
import os
from six.moves import cPickle as pickle

from keras import models
from keras import layers
from keras import optimizers
from keras import losses
from keras import metrics

data_root = 'D:\DemoDL\ASUdacity' # Change me to store data elsewhere
pickle_file = os.path.join(data_root, 'notMNIST.pickle')

with open(pickle_file, 'rb') as f:
   save = pickle.load(f)
   train_dataset = save['train_dataset']
   train_labels = save['train_labels']
   valid_dataset = save['valid_dataset']
   valid_labels = save['valid_labels']
   test_dataset = save['test_dataset']
   test_labels = save['test_labels']
   del save  # hint to help gc free up memory

num_labels = 10


def reformat(dataset, labels):
   labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
   return labels


train_labels = reformat(train_dataset, train_labels)
valid_labels = reformat(valid_dataset, valid_labels)
test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

batch_size = 128
image_size = 28
train_subset = 10000


def notmnist_net(epoch, lr):
    model = models.Sequential()
    model.add(layers.Dense(1024, activation='relu', input_shape=(784, )))
    model.add(layers.Dense(10, activation='softmax'))

    model.compile(optimizer=optimizers.SGD(lr=lr),
                 loss=losses.categorical_crossentropy,
                 metrics=[metrics.categorical_accuracy])

    history = model.fit(train_dataset, train_labels, epochs=epoch, batch_size=batch_size, validation_data=(valid_dataset, valid_labels))
    print(history.history)
    return model


model = notmnist_net(3, 0.1)
predictions = model.predict(test_dataset)
print(predictions[0])



