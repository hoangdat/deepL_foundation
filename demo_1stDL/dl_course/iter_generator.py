
class zrange:

    def __init__(self, n):
        self.n = n

    def __iter__(self):
        return zrange_iter(self.n)


class zrange_iter:

    def __init__(self, n):
        self.i = 0
        self.n = n

    def __iter__(self):

# Iterators are iterables too

# Adding this functions to make them so

        return self

    def __next__(self):
        if self.i < self.n:
            i = self.i
            self.i += 1
            return i
        else:
            raise StopIteration()


class yrange:
    def __init__(self, n):
            self.i = 0
            self.n = n

    def __iter__(self):
        return self

    def __next__(self):
        if self.i < self.n:
            i = self.i
            self.i += 1
            return i
        else:
            raise StopIteration()


def xrange(n):
    i = 0
    while i <= n:
        yield i
        i += 1


# z = zrange(5)
# print(list(z))
# iter_z = z.__iter__()
# print(iter_z.__next__())
# print(iter_z.__next__())
# print(iter_z.__next__())
#
#
# print(list(yrange(5)))
# x = xrange(3)
# print(x.__next__())
# print(x.__next__())
# print(x.__next__())
# print(x.__next__())
# print(x.__next__())
# print(x.__next__())
# print(x.__next__())
# print(x.__next__())
# print(x.__next__())

b = False


def foo():
    print("begin")
    for i in range(3):
        b = True
        print("before yield", i)
        yield i
        print("after yield", i)
    if b is False:
        print("yield test")
        yield b
    print("end")


f = foo()
print(f.__next__())
print(f.__next__())
print(f.__next__())
print(f.__next__())