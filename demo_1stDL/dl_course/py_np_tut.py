import numpy as np
import matplotlib.pyplot as plt

def quicksort(arr):
    if len(arr) < 1:
        return arr
    pivot = arr[len(arr) // 2]
    print("pivot: ", pivot)
    left = [x for x in arr if x < pivot]
    print("left: ", left)
    middle = [x for x in arr if x == pivot]
    print("middle: ", middle)
    right = [x for x in arr if x > pivot]
    print("right: ", right)
    print("=======================")
    return quicksort(left) + middle + quicksort(right)


# print(quicksort([3, 6, 8, 10, 1, 2, 1]))
# x = 5
# print(x / 2)
#
# hello = 'hello'
# world = 'world'
# hw12 = '%s %s %d' % (hello, world, 12)
# print(hw12)
#
# print(hello.capitalize())
# print(hello.upper())
# print(hello.rjust(6))
# print(('%d' % 1).rjust(6))
#
# num = list(range(5))
# print(num)
#
# # list comprehension
# arr = [x for x in num[:-1] if x % 2 == 0]
# print(arr)
#
# s = ['a', 'b', 'c', 'd', 'e']
# print(s)
#
# # dict comprehension
# dic_com = {s[index]: x for index, x in enumerate(num) if x % 2 == 0}
# print(dic_com)
#
# # tuple
# d = {(x, x + 1): x for x in range(10)}
# # print(d)
# t = (5, 6)
# print(d[t])
#
# a = np.array([[1,2,3,4], [5,6,7,8], [9,10,11,12]])
# row_r1 = a[1, :]
# print(row_r1)
#
# b = [r for r in a[1, :]]
# print(b)
#
# row_r2 = a[:-1, :]
# b_2 = [r for r in a[:-1, :]]
# print(b_2)
# # print(row_r2)
#
#
# # boolean array indexing
# bool_idx = (a > 2)
# print(bool_idx)
# print(a[bool_idx])


# array math
x = np.array([[1, 2],
              [3, 4]], dtype=np.float64)
y = np.array([[5, 6],
              [7, 8]], dtype=np.float64)

# print(x + y)
# print(np.add(x, y))
#
# print(x - y)
# print(np.subtract(x, y))


print(x * y)
print(np.multiply(x, y))
print(np.dot(x, y))
print(np.matmul(x, y))


# broadcasting
x = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
v = np.array([1, 0, 1])
# v = np.array([[1], [0], [1], [0]])
vv = np.tile(v, (4, 1))
print(vv)

print(x.ndim, "-", x.shape)
print(v.ndim, "-", v.shape)
y = x + vv  # Add x and vv element-wise
print(y)

# ==========================================
# same with
# y = np.empty_like(x)
#
# Add the vector v to each row of the matrix x with an explicit loop
# for i in range(4):
#     y[i, :] = x[i, :] + v
# ==========================================

# broadcast
# trailing axes
y_ = x + v
print(y_)


# application of broadcast
# Add a vector to each row of a matrix
# Add a vector to each column of a matrix
# Multiply a matrix by a constant



# plot
x = np.arange(0, 3 * np.pi, 0.1)
ysin = np.sin(x)
ycos = np.cos(x)


plt.plot(x, ycos, "b")
plt.plot(x, ysin, "g")
plt.xlabel('x axis label')
plt.ylabel('y axis label')
plt.title('Sine and Cosine')
# plt.legend(['Sine', 'Cosine'])
plt.show()






