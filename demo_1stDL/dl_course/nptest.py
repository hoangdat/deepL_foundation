import numpy as np
import math
X = np.array([147, 150, 153, 158, 163, 165, 168, 170, 173, 175, 178, 180, 183])
print(X[[2, 4, 5]])
X[[1, 3, 4]] = 1
print(X)


x = np.array([[1, 2], [3, 4], [5, 6]])
print(x)
index = np.array([[0, 1, 2], [0, 1, 0]])
print(index)
print(x[[0, 1, 2], [0, 1, 0]])
# y = np.array([[49, 50, 51,  54, 58, 59, 60, 62, 63, 64, 66, 67, 68]]).T
# print(y)
#
# one = np.ones((X.shape[0], 1))
# Xbar = np.concatenate((one, X), axis=1)
# print(Xbar)
# x = np.array([1., 2, 3])
# print(type(x))
# print(type(x[0]))
#
# arr_test = np.array([[1, 2, 3], [1, 2, 3], [1, 2, 3]])
# print(arr_test[[1, 0, 2], :])
# permutation = np.random.permutation(arr_test.shape[0])
# print(permutation)
# l = arr_test[permutation, :]
# print(l)
#
# z = np.zeros(3)
# print(z)
#
# z = np.zeros_like(x)
# print(z)
#
# a = np.arange(0, 1, 0.1)
# print(a)
#
# end = np.log2(1025)
# print(end)
# a = np.arange(0, end, dtype=np.int64)
# print(2**a)
#
# a = 3*np.ones(10)
# a[9] = 1.5
# print(a)

# a = np.ones((20, 3, 3))
# b = np.array([[4, 5, 6], [7, 8, 9], [10, 11, 12]])
# a[:-1, :, :] = b
# print("before: ", a)
# a = a.reshape((-1, 3 * 3)).astype(np.float32)
# print("after: ", a)

# a = np.array([1, 1, 1, 1])
# b = np.arange()



def my_avg(x):
    d = x.shape[0]
    average = np.sum(x)/d
    a = average * np.ones_like(x)
    return a


# x = np.array([1., 2, 3, 4, 5, 6, 7, 8, 9, 10])
# print(my_avg(x))


def gen_array(n):
    b = np.arange(2, 2-0.5*n, -0.5)
    a = np.ones(n)
    es = math.ceil(n/2)
    a[::2] = b[:es]
    a[1::2] = -1
    return a


# print(gen_array(12))

# def sum_array(x):
#     y = math.pi / 2 - x
#     z = np.cos(x) - np.sin(x)

def norm1(x):
    return np.sum(np.abs(x))


def softmax(x):
    return np.exp(x)/np.sum(np.exp(x))


def norm2(x):
    return math.sqrt(np.sum(x*x))


def softmax_stable(x):
    max_x = x.max()
    return np.exp(x - max_x)/np.sum(np.exp(x - max_x))


def softmax_stable_2(Z):
    """
    Compute softmax values for each sets of scores in Z.
    each column of Z is a set of score.
    """
    e_Z = np.exp(Z - np.max(Z, axis = 0, keepdims = True))
    A = e_Z / e_Z.sum(axis = 0)
    return A

#
# np.random.seed(2)
# offset = 1000
# z = np.random.randn(3) + offset
#
# print(softmax_stable(z))
# print(softmax_stable_2(z))


A = np.array([[-2, 2], [-2, 2]])
print(A)
print(A.T)
