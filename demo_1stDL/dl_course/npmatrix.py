import numpy as np
from math import sqrt

A = np.array([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]], np.float64)
print(A[:2, :])
print((np.product(A.shape), ))
print(np.reshape(A, (np.product(A.shape), )))


def gen_array_diag(n):
    di = np.arange(1, n + 1)
    a = np.diag(di, k=-1)
    return a


a = np.arange(1, 4)
# print(a)

# print(A[:, ::2])
# print(np.sum(A, axis=1, keepdims=True))
# print(np.sum(np.max(A, axis=1) - np.min(A, axis=1)))
# print(A[1] / 3)


def sum_even_col(A):
    shape = A.shape
    row = shape[0]
    cal = 0
    for i in range(row):
        cal += np.sum(A[i, ::2])
    return cal


def frobenius_norm(A):
    return sqrt(np.sum(A*A))


# np.random.seed(1)
# A = np.random.rand(10, 10)
# np.reshape()
# A = np.array([[1, 3], [2, 5]])
# print(2**A)
# print(frobenius_norm(A))
# print(sum_even_col(A))

#
A = np.array([[1, 2, 3],[4, 5, 6]])
print(A.T)
print(A.transpose())
B = np.reshape(A, (3, 2), order="C")
print(B)
#
#
# A = np.arange(1, 13)
# A = np.reshape(A, (3, 4))
# A = A.T
# A = np.reshape(A, (3, 4), order='C')
# print(A)


def zero_mean(x):
    print(x)
    col = x.shape[1]
    print(col)
    mean = np.sum(x/col, axis=1, keepdims=True)
    print(mean)
    return x - mean


# np.random.seed(1)
# X = np.random.rand(100, 128)
# # don't care about the following line
# # it's for checking result only
# print(np.sum(zero_mean(X)**2))
#
# A = np.array([[0,  1,  2],
#               [3,  4,  5],
#               [6,  7,  8],
#               [9, 10, 11]])
# b = np.array([1, 3, 4])
# print(A.shape, b.shape)
#
# print(np.exp(A - np.max(A, axis=1, keepdims=True)))


def full_soft_max(z):
    max_row = np.max(z, axis=1, keepdims=True)
    a_ = np.exp(z - max_row) / np.sum(np.exp(z - max_row), axis=1, keepdims=True)
    return a_


