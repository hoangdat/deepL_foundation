import os
import tensorflow as tf
from six.moves.urllib.request import urlretrieve
import pandas as pd


DATA_URL = "http://download.tensorflow.org/data/"

CSV_COLUMN_NAMES = ['SepalLength', 'SepalWidth',
                    'PetalLength', 'PetalWidth', 'Species']
SPECIES = ['Setosa', 'Versicolor', 'Virginica']

test_set_name = "iris_test.csv"
train_set_name = "iris_training.csv"

data_root = 'D:\DemoDL\learnTF'


def maybe_download_data():
    train_set = verified_set(train_set_name)
    test_set = verified_set(test_set_name)
    return train_set, test_set


def verified_set(set_name):
    set = os.path.join(data_root, set_name)
    if os.path.exists(set):
        print('Found and verified', set_name)
    else:
        set, _ = urlretrieve(DATA_URL + set_name, set);
        print("downloaed set: " + set_name)
    return set


def load_data(y_name='Species'):
    """Returns the iris dataset as (train_x, train_y), (test_x, test_y)."""
    train_path, test_path = maybe_download_data()

    train = pd.read_csv(train_path, names=CSV_COLUMN_NAMES, header=0)
    train_x, train_y = train, train.pop(y_name)

    test = pd.read_csv(test_path, names=CSV_COLUMN_NAMES, header=0)
    test_x, test_y = test, test.pop(y_name)

    return (train_x, train_y), (test_x, test_y)