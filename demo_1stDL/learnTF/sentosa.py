from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range

import matplotlib.pyplot as plt

import os
import sys

from IPython.display import display, Image

from scipy import ndimage

import random


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(1.0, shape=shape)
    return tf.Variable(initial)


def conv2d(data, weight):
    # strides [1, x_movement, y_movement, 1]
    return tf.nn.conv2d(data, weight, strides=[1, 1, 1, 1], padding='VALID')


def max_pooling(data):
    return tf.nn.max_pool(data, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')


def model(dataset):
    # conv1 layer 1
    hidden1 = tf.nn.relu(conv2d(dataset, layer1_weights) + layer1_biases)  # 28 * 28 * depth1
    pool1 = max_pooling(hidden1)  # 14 * 14 * depth1
    # conv2 layer 2
    hidden2 = tf.nn.relu(conv2d(pool1, layer2_weights) + layer2_biases)  # 10 * 10 * depth2
    pool2 = max_pooling(hidden2)  # 5 * 5 * depth2
    # conv3 layer 3
    pool3 = tf.nn.relu(conv2d(pool2, layer3_weights) + layer3_biases)  # 1 * 1 * depth3
    #         pool3 = max_pooling(hidden3) # 1 * 1 * depth3

    shape = pool3.get_shape().as_list()
    pool3_flat = tf.reshape(pool3, [shape[0], shape[1] * shape[2] * shape[3]])

    hidden4_drop = tf.nn.dropout(pool3_flat, 0.9375)

    logits_1 = tf.matmul(hidden4_drop, s1_w) + s1_b
    logits_2 = tf.matmul(hidden4_drop, s2_w) + s2_b
    logits_3 = tf.matmul(hidden4_drop, s3_w) + s3_b
    logits_4 = tf.matmul(hidden4_drop, s4_w) + s4_b
    logits_5 = tf.matmul(hidden4_drop, s5_w) + s5_b

    return [logits_1, logits_2, logits_3, logits_4, logits_5]

learning_rate = tf.train.exponential_decay

loss_per_digit = [tf.reduce_mean(
                        tf.nn.sparse_softmax_cross_entropy_with_logits(
                            logits[i],
                            tf_train_labels[:,i+1]
                        )) + beta_regul * tf.nn.l2_loss(sw[i])
                       for i in range(5)]