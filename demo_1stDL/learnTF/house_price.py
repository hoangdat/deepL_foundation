from keras.datasets import boston_housing
from keras import models
from keras import layers
from keras import optimizers
from keras import losses
from keras import metrics
import numpy as np

(train_data, train_label), (test_data, test_label) = boston_housing.load_data()

print(train_data.shape)

mean = train_data.mean(axis=0)
train_data -= mean
std = train_data.std(axis=0)

print(std)

train_data /= std

test_data -= mean
test_data /= mean


def build_model():
    model = models.Sequential()
    model.add(layers.Dense(64, activation='relu', input_shape=(train_data.shape[1],)))
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(1))

    model.compile(optimizer=optimizers.RMSprop,
                  loss=losses.mse,  # mean squared error widely use for regression
                  metrics=['mae'])
    return model


# k fold
k = 4
num_val_sample = len(train_data) // k
num_epochs = 100
all_scores = []

for i in range(k):
    print("processing fold #", i)
    val_data = train_data[i * num_val_sample: (i+1) * num_val_sample]
    val_labels = train_label[i * num_val_sample: (i+1) * num_val_sample]

    partial_train_data = np.concatenate([train_data[:i * num_val_sample], train_data[(i + 1) * num_val_sample:]], axis=0)
    partial_train_label = np.concatenate([train_label[:i * num_val_sample], train_label[(i + 1) * num_val_sample:]], axis=0)

    model = build_model()
    model.fit(partial_train_data, partial_train_label, epochs=num_epochs, batch_size=1, verbose=0)

    val_mse, val_mae = model.evaluate(val_data, val_labels, verbose=0)
    all_scores.append(val_mae)


