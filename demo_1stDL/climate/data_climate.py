import os
import numpy as np
from matplotlib import pyplot as plt

data_dir = 'D:\\DemoDL\\jena_climate'
fname = os.path.join(data_dir, 'jena_climate_2009_2016.csv')

f = open(fname)
data = f.read()
f.close()

lines = data.split('\n')
# print(lines[0])
header = lines[0].split(',')
lines = lines[1:]

print(header)
print(len(lines))

# print(lines[1])

float_data = np.zeros((len(lines), len(header) - 1))
for i, line in enumerate(lines):
    values = [float(x) for x in line.split(',')[1:]]
    float_data[i, :] = values

# temp = float_data[:, 1]
# items_count = 1440
# plt.plot(range(items_count), temp[:items_count])
# plt.show()

# normalizing data
count_item_use = 200000;
# float_1 = float_data[2000 + 144][1]
mean = float_data[:count_item_use].mean(axis=0)
# print(mean.shape)
# mean_1 = mean[1]
float_data -= mean
std = float_data[:count_item_use].std(axis=0)
# std_1 = std[1]
float_data /= std

# a = range(2000 - 1024, 2000, 6)
# s = float_data[a]
# t = float_data[2000 + 144][1]
# print(s)
# print(t)
# print('--------------')
# print(float_1, '---m1: ', mean_1, '---s1: ', std_1, '---r = ', ((float_1 - mean_1)/std_1))


def generator(data, lookback, delay, min_index, max_index,
              shuffle=False, batch_size=128, step=6):
    if max_index is None:
        max_index = len(data) - delay - 1
    i = min_index + lookback
    while 1:
        if shuffle:
            rows = np.random.randint(
                min_index + lookback, max_index, size=batch_size)
        else:
            if i + batch_size >= max_index:
                i = min_index + lookback
            rows = np.arange(i, min(i + batch_size, max_index))
            i += len(rows)

        samples = np.zeros((len(rows), lookback // step,
                            data.shape[-1]))
        targets = np.zeros((len(rows),))
        for j, row in enumerate(rows):
            indices = range(rows[j] - lookback, rows[j], step)
            samples[j] = data[indices]
            targets[j] = data[rows[j] + delay][1]
        yield samples, targets


lookback = 1440
step = 6
delay = 144
batch_size = 128

train_gen = generator(float_data,
                      lookback=lookback,
                      delay=delay,
                      min_index=0,
                      max_index=200000,
                      shuffle=True,
                      step=step,
                      batch_size=batch_size)

val_gen = generator(float_data,
                    lookback=lookback,
                    delay=delay,
                    min_index=200001,
                    max_index=300000,
                    step=step,
                    batch_size=batch_size)

test_gen = generator(float_data,
                    lookback=lookback,
                    delay=delay,
                    min_index=300001,
                    max_index=None,
                    step=step,
                    batch_size=batch_size)


# def evaluate_naive_method():
#     batch_maes = []
#     for step in range(val_steps):
#         samples, targets = next(val_gen)
#         preds = samples[:, -1, 1]
#         mae = np.mean(np.abs(preds - targets))
#         batch_maes.append(mae)
#     print(np.mean(batch_maes))
