import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import tensorflow as tf


def unpickle(file):
    import pickle
    with open(file, "rb") as fo:
        dict_ret = pickle.load(fo, encoding='bytes')

    return dict_ret


def pre_processing_image(image, training):
    if training:
        image = tf.random_crop(image, size=[32, 32, 3])
        image = tf.image.random_flip_left_right(image)
        image = tf.image.random_hue(image, max_delta=0.05)
        image = tf.image.random_contrast(image, lower=0.3, upper=1.0)
        image = tf.image.random_brightness(image, max_delta=0.2)
        image = tf.image.random_saturation(image, lower=0.0, upper=2.0)
        # image = tf.minimum(image, 1.0)
        # image = tf.maximum(image, 0.0)
    else:
        image = tf.image.resize_image_with_crop_or_pad(image, target_height=32, target_width=32)

    return image


dict1 = unpickle("data_batch_1")
# print(dict1)
# print(dict1[b'labels'])
image1 = dict1[b'data'][0]
img = np.array(image1[:3072])
imag = img.reshape(3, 32, 32)
img_view = np.moveaxis(imag, 0, 2)
plt.imshow(img_view)
with tf.Session() as sess:
    image_ = sess.run(pre_processing_image(img_view, True))
    print(image_)
    plt.imshow(image_)
    plt.show()
# print(image1)
# img = np.array(image1[:3072])
# print(img, "-", img[:32])
# image2 = dict1[b'data'][2][:1024]
# # print(img.ndim)
# # print(img.size)
# imag = img.reshape(3, 32, 32)
# print(imag.shape)
# img_view = np.moveaxis(imag, 0, 2)
# # print(imag)
# plt.imsave('1.png', img_view)
# plt.imshow(img_view)
# plt.show()


# arr1d = np.arange(24)
# print(arr1d)
# arr3d = arr1d.reshape(4, 3, 2).transpose()
# print(arr3d)
# print(arr3d[1, 2, 0])
# # plt.imshow(arr3d, cmap='gray')
# # plt.show()
# a1x2 = np.arange(2)
# print(a1x2)
# a2x1 = np.arange(2).reshape(2, 1)
# print(a2x1)
# print(a2x1[1])
# print("xxxxxxxx")
# a1x2x2 = np.arange(4).reshape(1, 2, 2)
# print(a1x2x2)
# print(a1x2x2[0, 0, 0])
# print(a1x2x2[0, 0, 1])
# print(a1x2x2[0, 1, 0])
# print(a1x2x2[0, 1, 1])
# a1x3x2 = np.arange(6).reshape(1, 3, 2)
# print("xxxxxxxx")
# print(a1x3x2)
# print(a1x3x2[0, 2, 1])
# a1x2x3 = np.arange(6).reshape(1, 2, 3)
# print(a1x2x3)

# a = np.array([[0, 0, 0, 1, 1, 1]])
# b = a.reshape(1, 2, 3)
# print(b)
# b = a.reshape(2, 3, 1)
# print(b)

