import matplotlib.pyplot as plt
import numpy as np
import digit_detector.file_io as file_io
import os

DIR = 'D:\WorkspacePython\SVHN-deep-digit-detector-master\sample_loader'
NB_FILTERS = 32
NB_EPOCH = 5


def show_image(img, label):
    print("Labels", label)
    print("Dtype", img.dtype)
    print("Shape", img.shape)
    print("Color range", np.min(img), np.max(img))
    if len(img.shape) > 2:
        plt.imshow(np.reshape(img[:, :, 1], img.shape[:2]))
    else:
        plt.imshow(img)
    plt.show()


def show_images(imgs, labels, num=3):
    for i in range(num):
        num = np.random.randint(imgs.shape[0])
        show_image(imgs[num], labels[num])


if __name__ == "__main__":
    # (457723, 32, 32, 3) (457723, 1)
    # (113430, 32, 32, 3) (113430, 1)
    images_train = file_io.FileHDF5().read(os.path.join(DIR, "train.hdf5"), "images")
    labels_train = file_io.FileHDF5().read(os.path.join(DIR, "train.hdf5"), "labels")
    print(labels_train.shape)
    # images_val = file_io.FileHDF5().read(os.path.join(DIR, "val.hdf5"), "images")
    # labels_val = file_io.FileHDF5().read(os.path.join(DIR, "val.hdf5"), "labels")

    index = np.random.randint(1012500, labels_train.shape[0] - 1, 10)
    print(index)
    imgs = images_train[index]
    labels = labels_train[index]
    print(labels)
    show_images(imgs, labels, 10)
    print("done!!!")

