#-*- coding: utf-8 -*-

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

import digit_detector.extractor as extractor_
import digit_detector.file_io as file_io
import digit_detector.annotation as ann
from digit_detector import region_proposal as rp

N_IMAGES = None
DIR_TRAIN = 'D:\\WorkspacePython\\demo_1stDL\\svhn\\train'
ANNOTATION_FILE = "D:\\WorkspacePython\\demo_1stDL\\svhn\\train\\digitStruct.json"
NEG_OVERLAP_THD = 0.05
POS_OVERLAP_THD = 0.6
PATCH_SIZE = (32, 32)

if __name__ == "__main__":

    # 1. file load
    files = file_io.list_files(directory=DIR_TRAIN, pattern="*.png", recursive_option=False, n_files_to_sample=N_IMAGES, random_order=False)
    n_files = int(len(files) * 0.3)
    n_train_files = int(n_files * 0.8)
    print(n_train_files)
    annotator = ann.SvhnAnnotation(ANNOTATION_FILE)

    extractor = extractor_.Extractor(rp.kMserRegionProposer(), annotator, rp.OverlapCalculator())
    train_samples, train_labels = extractor.extract_patch(files[:n_train_files], PATCH_SIZE, POS_OVERLAP_THD, NEG_OVERLAP_THD)

    extractor = extractor_.Extractor(rp.kMserRegionProposer(), annotator, rp.OverlapCalculator())
    validation_samples, validation_labels = extractor.extract_patch(files[n_train_files:n_files], PATCH_SIZE, POS_OVERLAP_THD, NEG_OVERLAP_THD)

    print(train_samples.shape, train_labels.shape, train_samples.dtype)
    print(validation_samples.shape, validation_labels.shape, validation_samples.dtype)
      
#     show.plot_images(samples, labels.reshape(-1,).tolist())

    train_file = file_io.FileHDF5("train.hdf5", "w")
    train_file.write(train_samples, "train.hdf5", "images", dtype="uint8")
    train_file.write(train_labels, "train.hdf5", "labels", dtype="int")

    val_file = file_io.FileHDF5("val.hdf5", "w")
    val_file.write(validation_samples, "val.hdf5", "images", dtype="uint8")
    val_file.write(validation_labels, "val.hdf5", "labels", dtype="int")
     
    # (457723, 32, 32, 3) (457723, 1)
    # (113430, 32, 32, 3) (113430, 1)




